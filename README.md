# Visualiseur des données

Widget de prévisualisation des données issues de fichiers csv

---

## Résumé

Afin de pouvoir partager et de mettre en valeur toutes les ressources de ce repo il a été proposé de créer un outil numérique de type "widget" : `datami`. En effet un outil de ce type permet de pouvoir intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... gratuitement.

La solution proposée et réalisée ici s'appuie sur un projet open source porté par la coopérative numérique [**multi**](https://multi.coop) : le projet [Datami](https://datami.multi.coop).

---

## Démo

- Page html de démo : [![Netlify Status](https://api.netlify.com/api/v1/badges/09ad38cb-a406-4893-bb68-1c1178dc9f4f/deploy-status)](https://app.netlify.com/sites/etude-ifn-parentalite/deploys)
- url de démo : https://etude-ifn-parentalite.netlify.app

---

## Pour aller plus loin

### Datami

Le widget fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)

### Documentation technique

Un site dédié à la doucmentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Mini server for local development

A mini server is writen in the `server.py` to serve this folder's files.

To install the mini-server :

```sh
pip install --upgrade pip
python3 -m pip install --user virtualenv
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

or

```sh
sh setup.sh
source venv/bin/activate
```

---

### Run local server

To run the server on `http://localhost:8810`:

```sh
python server.py
```

or

```sh
sh run_server.sh
```

Files will be locally served on :

- `http://localhost:8810/content/<path:folder_path>/<string:filename>`
- `http://localhost:8810/statics/<path:folder_path>/<string:filename>`


---

### Geocoder (experimental)

To geocode the `data_commune.csv` dataset :

Install dependencies if not already done :

```sh
sh setup.sh
```

then

```sh
source venv/bin/activate
python geocoder.py "data/data_commune.csv" -sep ";" -cols_a libelle_commune code_departement libelle_departement -out data -debug true
```

or

```sh
sh run_geocoding.sh
```

The output geocoded file will be generated at `data/data_commune-geocoded.csv`

---

## Crédits

| | logo | url |
| :-: | :-: | :-: |
| **La Trousse à Projets** | ![La trousse à projets](./images/trousse-a-projets-logo.png) | https://trousseaprojets.fr |
| **La Mednum** | ![mednum](./images/mednum-logo.png) | https://lamednum.coop |
| **coopérative numérique multi** | ![multi](./images/multi-logo.png) | https://multi.coop |
